## Dataset 13: *Banana streak virus*

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/VtNlbJxVjOq8ygr-00YrtkRtePICf1Uva2SFlrYM2B4).

*Table 1: Composition of dataset 13. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     DQ451009      	|     Artificial    	|     6950                  	|     35834                               	|     1396                                              	|     37.93                                     	|
|     KT895259      	|     Artificial    	|     7450                  	|     14308                               	|     557                                               	|     15.14                                     	|
|     DQ092436      	|     Artificial    	|     7722                  	|     14892                               	|     580                                               	|     15.76                                     	|
|     AY750155      	|     Artificial    	|     7801                  	|     10140                               	|     395                                               	|     10.73                                     	|
|     KT895258      	|     Artificial    	|     7769                  	|     9946                                	|     387                                               	|     10.53                                     	|
|     AY493509      	|     Artificial    	|     7263                  	|     9360                                	|     365                                               	|     9.91                                      	|


*Table 2: Percentage identity between the strains.*

|                       	|     AY493509.1    	|     AY750155.1    	|     DQ092436.1    	|     DQ451009.1    	|     KT895258.1    	|     KT895259.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|-------------------	|-------------------	|-------------------	|
|     **AY493509.1**    	|     100           	|     55.92         	|     56.416        	|     56.031        	|     54.885        	|     56.162        	|
|     **AY750155.1**    	|     55.92         	|     100           	|     85.965        	|     59.486        	|     70.11         	|     60.294        	|
|     **DQ092436.1**    	|     56.416        	|     85.965        	|     100           	|     59.986        	|     70.938        	|     60.605        	|
|     **DQ451009.1**    	|     56.031        	|     59.486        	|     59.986        	|     100           	|     60.052        	|     75.537        	|
|     **KT895258.1**    	|     54.885        	|     70.11         	|     70.938        	|     60.052        	|     100           	|     59.459        	|
|     **KT895259.1**    	|     56.162        	|     60.294        	|     60.605        	|     75.537        	|     59.459        	|     100           	|
