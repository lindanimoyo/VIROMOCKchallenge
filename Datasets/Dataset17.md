## Dataset 17: *Little cherry virus 1* (LChV1)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/1VnxLndGgensb0UoNU5aq2tOc26oLmWRE7rChgzgNcE).

*Table 1: Composition of dataset 17. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     MH300061      	|     Artificial    	|     16963                 	|     39890                               	|     704                                               	|     40.28                                     	|
|     KX192366      	|     Artificial    	|     16933                 	|     19712                               	|     348                                               	|     19.91                                     	|
|     EU715989      	|     Artificial    	|     16936                 	|     14784                               	|     261                                               	|     14.93                                     	|
|     LN794218      	|     Artificial    	|     16880                 	|     14784                               	|     261                                               	|     14.93                                     	|
|     MG934545      	|     Artificial    	|     16927                 	|     9856                                	|     174                                               	|     9.95                                      	|

*Table 2: Percentage identity between the strains.*

|                       	|     EU715989.1    	|     KX192366.1    	|     LN794218.1    	|     MG934545.1    	|     MH300061.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|-------------------	|-------------------	|
|     **EU715989.1**    	|     100           	|     76.369        	|     72.692        	|     75.522        	|     75.704        	|
|     **KX192366.1**    	|     76.369        	|     100           	|     73.511        	|     77.188        	|     77.956        	|
|     **LN794218.1**    	|     72.692        	|     73.511        	|     100           	|     73.498        	|     73.336        	|
|     **MG934545.1**    	|     75.522        	|     77.188        	|     73.498        	|     100           	|     77.243        	|
|     **MH300061.1**    	|     75.704        	|     77.956        	|     73.336        	|     77.243        	|     100           	|
